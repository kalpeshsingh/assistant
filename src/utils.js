export function playSound(file) {
  var sound = new Audio(file);
  sound.play();
}
export function tagsGenerator(task) {
  var tags = task.name.match(/\[\w+]/g);
  if (tags) {
    var formattedTags = tags.map(function(tag, idx) {
      task.name = task.name.replace(tag, "").trim();
      return tag.replace("[", "").replace("]", "");
    });
    task.tags = formattedTags;
  }
}
