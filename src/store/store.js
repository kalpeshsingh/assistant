import Vue from "vue";
import Vuex from "vuex";
import * as utils from "../utils";
import moment from "moment";

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    taskInput: "",
    taskFeedback: "",
    taskCollection: boostrappedData,
    preferences: preferences
  },
  getters: {
    isEmpty: state => {
      return state.taskInput.length <= 0;
    },
    hasTaskCollection: state => {
      return state.taskCollection.length > 0;
    },
    taskCompletionMeta: state => {
      var completedTaskCollection, computedTaskPercentage;

      completedTaskCollection = state.taskCollection.filter(function(task) {
        return task.isCompleted;
      });

      computedTaskPercentage =
        state.taskCollection.length > 0
          ? (
              completedTaskCollection.length / state.taskCollection.length
            ).toFixed(2) * 100
          : 0;

      return {
        completedTaskCount: completedTaskCollection.length,
        totalTaskCount: state.taskCollection.length,
        taskCompletedPercentage: computedTaskPercentage.toFixed(0)
      };
    }
  },
  mutations: {
    updateTaskInput(state, value) {
      state.taskInput = value;
    },
    addTask(state) {
      var data = {
        id: moment().unix(),
        name: state.taskInput,
        isCompleted: false,
        timestamp: moment().format("ddd, Do MMM, hh:mm A"),
        sTimestamp: moment().format("HH:mm"),
        tags: ""
      };
      utils.tagsGenerator(data);
      boostrappedData.unshift(data);
      addTask(data);
      utils.playSound(require("@/assets/sounds/task_added.m4a"));
      state.taskFeedback =
        "Task " + state.taskInput + " has been added in your task list";
      state.taskInput = "";
    },
    deleteTask: function(state, { index, id, name }) {
      state.taskCollection.splice(index, 1);
      state.taskFeedback =
        "Task " + name + " has been deleted from your task list";
      deleteTask(id);
      utils.playSound(require("@/assets/sounds/task_deleted.m4a"));
    },
    isTaskCompleted: function(state, { idx, id, status }) {
      state.taskCollection[idx]["isCompleted"] = status;
      if (status) {
        state.taskFeedback =
          "Task " +
          state.taskCollection[idx]["name"] +
          " has been marked as completed";
      }
      markTask(id, status);
      utils.playSound(require("@/assets/sounds/task_completed.m4a"));
    },
    updatePreferences: function(state, data) {
      state.preferences.findIndex(pref => {
        if (pref.name === data.name) {
          pref.name = data.name;
          pref.status = data.status;
        }
      });
      updatePreferences({
        name: data.name,
        status: data.status
      });
    }
  }
});
