/**
 * Created by kalpesh on 24/06/17.
 */

//prefixes of indexedDB implementation in chrome
window.indexedDB = window.indexedDB || window.webkitIndexedDB;

//prefixes of window.IDB objects
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange;

//console error for unstable version of IndexedDB
if (!window.indexedDB) {
  console.error("Your browser doesn't support a stable version of IndexedDB.");
}

const databaseName = "assistantData";
const databaseVersion = 2;
const objectStoreTask = "tasks";
const objectStorePrefs = "preferences";
var db;
var boostrappedData = [];
var preferences = [];

var request = window.indexedDB.open(databaseName, databaseVersion);

request.onerror = function() {
  console.error("ERROR: Seems like you lost in milky wave. " + request.error);
};

request.onupgradeneeded = function(event) {
  var _db = request.result;
  if (!_db.objectStoreNames.contains(objectStoreTask)) {
    _db.createObjectStore(objectStoreTask, { keyPath: "id" });
  }
  if (event.oldVersion < databaseVersion) {
    if (!_db.objectStoreNames.contains(objectStorePrefs)) {
      _db.createObjectStore(objectStorePrefs, { keyPath: "name" });
    }
  }
};

request.onsuccess = function(event) {
  db = event.target.result;

  var transaction = db.transaction([objectStorePrefs], "readwrite");
  var pref = transaction.objectStore(objectStorePrefs);
  pref.add({
    name: "blur",
    status: false
  });

  loadTask(db);
  loadPreferences(db);
};

function loadTask(db) {
  var transaction = db.transaction([objectStoreTask], "readonly");
  var objectStore = transaction.objectStore(objectStoreTask);
  var cursor = objectStore.openCursor();

  cursor.onsuccess = function(e) {
    var res = e.target.result;
    if (res) {
      boostrappedData.unshift(res.value);
      res.continue();
    }
    return boostrappedData;
  };
}

function addTask(data) {
  var transaction = db.transaction([objectStoreTask], "readwrite");
  var task = transaction.objectStore(objectStoreTask);
  var requestAdd = task.add(data);
  requestAdd.onerror = function(e) {
    console.error("Error", e.target.error.name);
  };
  requestAdd.onsuccess = function(e) {
    console.info("Task is added!");
  };
}

function loadPreferences(db) {
  var transaction = db.transaction([objectStorePrefs], "readonly");
  var objectStore = transaction.objectStore(objectStorePrefs);
  var cursor = objectStore.openCursor();

  cursor.onsuccess = function(e) {
    var res = e.target.result;
    if (res) {
      preferences.push(res.value);
      res.continue();
    }
    return preferences;
  };
}

function updatePreferences(preference) {
  var transaction = db.transaction([objectStorePrefs], "readwrite");
  var preferenceStore = transaction.objectStore(objectStorePrefs);
  preferenceStore.openCursor().onsuccess = function(e) {
    var cursor = e.target.result;
    if (cursor) {
      if (cursor.value.name === preference.name) {
        var updateValue = cursor.value;
        updateValue.status = preference.status;
        var request = cursor.update(updateValue);
        request.onsuccess = function() {
          console.log(
            `Preference ${preference.name} updated to ${preference.status}`
          );
        };
      }
      cursor.continue();
    }
  };
}

function deleteTask(id) {
  var transaction = db.transaction([objectStoreTask], "readwrite");
  var task = transaction.objectStore(objectStoreTask);
  var requestAdd = task.delete(id);
  requestAdd.onerror = function(e) {
    console.error("Error", e.target.error.name);
  };
  requestAdd.onsuccess = function(e) {
    console.info("Task is deleted!");
  };
}

function markTask(id, status) {
  var transaction = db.transaction([objectStoreTask], "readwrite");
  var task = transaction.objectStore(objectStoreTask);

  task.openCursor().onsuccess = function(e) {
    var cursor = e.target.result;
    if (cursor) {
      if (cursor.value.id === id) {
        var updateData = cursor.value;
        updateData.isCompleted = status;
        var request = cursor.update(updateData);
        request.onsuccess = function() {
          console.log(`ID ${id} marked as completed`);
        };
      }
      cursor.continue();
    }
  };
}
